# C# WPF File manager

The file manager inspired by file commander that provides following features:
*  exploring Windows file system
*  creating, deleting, coping and moving files and directories
*  searching for files and directories by name
*  sorting by name and date (directories first)
*  bult-in text documents and images viewer

# Screenshots:

![screenshot-1](ProjektProgramowanieObiektowe/Images/screenshot1.png)
![screenshot-2](ProjektProgramowanieObiektowe/Images/screenshot2.png)
![screenshot-3](ProjektProgramowanieObiektowe/Images/screenshot3.png)
![screenshot-4](ProjektProgramowanieObiektowe/Images/screenshot4.png)