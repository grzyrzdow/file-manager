﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DataModels;

namespace ProjektProgramowanieObiektowe.Views
{
    /// <summary>
    /// Logika interakcji dla klasy TextAndImageFileView.xaml
    /// </summary>
    public partial class TextAndImageFileView : UserControl
    {
        DiscElement discElement;
        public TextAndImageFileView(DiscElement discElement)
        {
            InitializeComponent();
            this.discElement = discElement;
            fileName.Text = discElement.Name;
            creationTime.Text = discElement.CreationTime.ToString();
            if (discElement is MyDirectory)
            {
                image.Source = new BitmapImage(new Uri("C:\\Users\\Grzyrzdow\\Desktop\\ProjektProgramowanieObiektowe\\ProjektProgramowanieObiektowe\\Images\\folderimage.png"));
            }
            else
            {
                if (discElement.GetExtension() == ".txt")
                {
                    image.Source = new BitmapImage(new Uri("C:\\Users\\Grzyrzdow\\Desktop\\ProjektProgramowanieObiektowe\\ProjektProgramowanieObiektowe\\Images\\textimage.png"));
                }
                else
                {
                    image.Source = new BitmapImage(new Uri("C:\\Users\\Grzyrzdow\\Desktop\\ProjektProgramowanieObiektowe\\ProjektProgramowanieObiektowe\\Images\\imagefileimage.ico"));
                }
            }
        }
        //<summary>
        //Zmiana podświetlenia, gdy mysz jest nad nazwą bierzącego pliku
        //</summary>
        private void ChangeColor(object sender, MouseEventArgs e)
        {
            if (grid.Background != Brushes.DodgerBlue)
                grid.Background = Brushes.LightBlue;
        }
        //<summary>
        //Zmiana podświetlenia, gdy mysz już nie jest nad nazwą bierzącego pliku
        //</summary>
        private void ChangeColorBack(object sender, MouseEventArgs e)
        {
            if (grid.Background != Brushes.Red)
                grid.Background = Brushes.White;
        }

        public delegate void OpenFileEvent(DiscElement discElement);
        public event OpenFileEvent fileOpened;

        private void fileClick(object sender, MouseButtonEventArgs e)
        {
            grid.Background = Brushes.DodgerBlue;
            if (e.ClickCount == 2)
            {
                if (fileOpened != null)
                {
                    fileOpened.Invoke(discElement);
                }
            }
        }
    }
}
