﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DataModels;
using System.Diagnostics;
using System.IO;
using Microsoft.VisualBasic.FileIO;

namespace ProjektProgramowanieObiektowe.Views
{
    /// <summary>
    /// Logika interakcji dla klasy DiscElementView.xaml
    /// </summary>
    public partial class DiscElementView : UserControl
    {
        DiscElement discElement;

        public DiscElement DiscElement { get => discElement; set => discElement = value; }

        public DiscElementView(DiscElement discElement)
        {
            InitializeComponent();
            this.DiscElement = discElement;
            fileName.Text = discElement.Name;
            creationTime.Text = discElement.CreationTime.ToString();
            if (discElement is MyDirectory)
            {
                image.Source = new BitmapImage(new Uri("C:\\Users\\Grzyrzdow\\Desktop\\ProjektProgramowanieObiektowe\\ProjektProgramowanieObiektowe\\Images\\folderimage.png"));
            }
            else
            {                
                if(discElement.GetExtension() == ".txt")
                {
                    image.Source = new BitmapImage(new Uri("C:\\Users\\Grzyrzdow\\Desktop\\ProjektProgramowanieObiektowe\\ProjektProgramowanieObiektowe\\Images\\textimage.png"));
                }
                else if (discElement.GetExtension() == ".png" || discElement.GetExtension() == ".jpg" || discElement.GetExtension() == ".bmp" || discElement.GetExtension() == ".ico")
                {
                    image.Source = new BitmapImage(new Uri("C:\\Users\\Grzyrzdow\\Desktop\\ProjektProgramowanieObiektowe\\ProjektProgramowanieObiektowe\\Images\\imagefileimage.ico"));
                }
                else
                {
                    image.Source = new BitmapImage(new Uri("C:\\Users\\Grzyrzdow\\Desktop\\ProjektProgramowanieObiektowe\\ProjektProgramowanieObiektowe\\Images\\fileimage.png"));
                }
            }
        }

        public delegate void DirOpenDeleteCopyCheckEvent(DiscElement discElement);
        public event DirOpenDeleteCopyCheckEvent dirOpened;
        public event DirOpenDeleteCopyCheckEvent dirDeleted;
        public event DirOpenDeleteCopyCheckEvent dirCopied;
        public event DirOpenDeleteCopyCheckEvent dirChecked;
        public event DirOpenDeleteCopyCheckEvent txtFileOpened;


        //<summary>
        //Otwiera folder lub plik
        //</summary>
        private void fileClick(object sender, MouseButtonEventArgs e)
        {
            grid.Background = Brushes.DodgerBlue;
            if (e.ClickCount == 2)
            {
                if (DiscElement is MyFile)
                {
                    Process.Start(DiscElement.Path);
                }
                else
                {
                    if (dirOpened != null)
                    {
                        dirOpened.Invoke(DiscElement);
                    }
                }
            }
        }
        //<summary>
        //Zmiana podświetlenia, gdy mysz jest nad nazwą bierzącego pliku
        //</summary>
        private void ChangeColor(object sender, MouseEventArgs e)
        {
            if(grid.Background != Brushes.LightGreen && grid.Background != Brushes.DodgerBlue)
                grid.Background = Brushes.LightBlue;
        }
        //<summary>
        //Zmiana podświetlenia, gdy mysz już nie jest nad nazwą bierzącego pliku
        //</summary>
        private void ChangeColorBack(object sender, MouseEventArgs e)
        {
            if(grid.Background != Brushes.LightGreen)
                grid.Background = Brushes.White;
        }

        private void checker_Clicked(object sender, RoutedEventArgs e)
        {
            if(dirChecked != null)
            {
                dirChecked.Invoke(discElement);
            }
        }

        private void ShowContextMenu(object sender, MouseButtonEventArgs e)
        {
            ContextMenu contextMenu = new ContextMenu();
            contextMenu.Closed += ContextMenu_Closed;

            if (discElement.GetExtension() == ".txt" || discElement.GetExtension() == ".png" || discElement.GetExtension() == ".jpg" || discElement.GetExtension() == ".bmp" || discElement.GetExtension() == ".ico")
            {
                MenuItem openOption = new MenuItem();
                openOption.Header = "Otwórz";
                openOption.Click += OpenOption_Click;
                contextMenu.Items.Add(openOption);
            }
            MenuItem copyOption= new MenuItem();
            MenuItem deleteOption = new MenuItem();
            copyOption.Header = "Kopiuj";
            deleteOption.Header = "Usuń";
            copyOption.Click += CopyOption_Click;
            deleteOption.Click += DeleteOption_Click;
            contextMenu.Items.Add(copyOption);
            contextMenu.Items.Add(deleteOption);
            contextMenu.IsOpen = true;
            grid.Background = Brushes.DodgerBlue;
        }

        private void ContextMenu_Closed(object sender, RoutedEventArgs e)
        {
            grid.Background = Brushes.White;
        }

        private void DeleteOption_Click(object sender, RoutedEventArgs e)
        {
            if (DiscElement is MyFile)
            {
                try
                {
                    FileSystem.DeleteFile(DiscElement.Path, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin);
                }
                catch (Exception)
                {
                    MessageBox.Show("Nie można usunąć!");
                }
            }
            else
            {
                try
                {
                    FileSystem.DeleteDirectory(DiscElement.Path, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin);
                }
                catch (Exception)
                {
                    MessageBox.Show("Nie można usunąć!");
                }
            }
            if (dirDeleted != null)
            {
                dirDeleted.Invoke(DiscElement);
            }
        }

        private void CopyOption_Click(object sender, RoutedEventArgs e)
        {
            if (dirCopied != null)
            {
                dirCopied.Invoke(DiscElement);
            }
        }

        private void OpenOption_Click(object sender, RoutedEventArgs e)
        {
            if(txtFileOpened != null)
            {
                txtFileOpened.Invoke(discElement);
            }
        }
    }
}
