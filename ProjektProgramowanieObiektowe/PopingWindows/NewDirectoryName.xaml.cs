﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjektProgramowanieObiektowe.PopingWindows
{
    /// <summary>
    /// Logika interakcji dla klasy NewDirectoryName.xaml
    /// </summary>
    public partial class NewDirectoryName : Window
    {
        public NewDirectoryName()
        {
            InitializeComponent();
        }

        //<summary>
        //Czyścci text box, gdy klikamy na niego
        //</summary>
        private void ClearText(object sender, MouseButtonEventArgs e)
        {
            inputName.Text = "";
        }

        public delegate void InputNameEvent(string name);
        public event InputNameEvent nameGot;

        private void confirm_Click(object sender, RoutedEventArgs e)
        {
            if (nameGot!=null)
            {
                nameGot.Invoke(inputName.Text);
            }
        }
    }
}
