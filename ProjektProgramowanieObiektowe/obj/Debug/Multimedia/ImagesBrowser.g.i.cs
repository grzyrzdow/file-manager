﻿#pragma checksum "..\..\..\Multimedia\ImagesBrowser.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "37BDA202980DDA8762B5979F4D6C6C560E75EF86"
//------------------------------------------------------------------------------
// <auto-generated>
//     Ten kod został wygenerowany przez narzędzie.
//     Wersja wykonawcza:4.0.30319.42000
//
//     Zmiany w tym pliku mogą spowodować nieprawidłowe zachowanie i zostaną utracone, jeśli
//     kod zostanie ponownie wygenerowany.
// </auto-generated>
//------------------------------------------------------------------------------

using ProjektProgramowanieObiektowe.Multimedia;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace ProjektProgramowanieObiektowe.Multimedia {
    
    
    /// <summary>
    /// ImagesBrowser
    /// </summary>
    public partial class ImagesBrowser : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 11 "..\..\..\Multimedia\ImagesBrowser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel imageFileList;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\..\Multimedia\ImagesBrowser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock imageFilespath;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\Multimedia\ImagesBrowser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock currentImageFileName;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\Multimedia\ImagesBrowser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button backButton;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\Multimedia\ImagesBrowser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imageContent;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\Multimedia\ImagesBrowser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox drivesList;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ProjektProgramowanieObiektowe;component/multimedia/imagesbrowser.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Multimedia\ImagesBrowser.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.imageFileList = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 2:
            this.imageFilespath = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.currentImageFileName = ((System.Windows.Controls.TextBlock)(target));
            
            #line 14 "..\..\..\Multimedia\ImagesBrowser.xaml"
            this.currentImageFileName.MouseEnter += new System.Windows.Input.MouseEventHandler(this.ChangeColor);
            
            #line default
            #line hidden
            
            #line 14 "..\..\..\Multimedia\ImagesBrowser.xaml"
            this.currentImageFileName.MouseLeave += new System.Windows.Input.MouseEventHandler(this.ChangeColorBack);
            
            #line default
            #line hidden
            
            #line 14 "..\..\..\Multimedia\ImagesBrowser.xaml"
            this.currentImageFileName.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(this.OpenImageDirectory);
            
            #line default
            #line hidden
            
            #line 14 "..\..\..\Multimedia\ImagesBrowser.xaml"
            this.currentImageFileName.PreviewMouseUp += new System.Windows.Input.MouseButtonEventHandler(this.OpenImageDirectory);
            
            #line default
            #line hidden
            return;
            case 4:
            this.backButton = ((System.Windows.Controls.Button)(target));
            
            #line 15 "..\..\..\Multimedia\ImagesBrowser.xaml"
            this.backButton.Click += new System.Windows.RoutedEventHandler(this.backButton_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.imageContent = ((System.Windows.Controls.Image)(target));
            return;
            case 6:
            this.drivesList = ((System.Windows.Controls.ComboBox)(target));
            
            #line 17 "..\..\..\Multimedia\ImagesBrowser.xaml"
            this.drivesList.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.drivesList_SelectionChanged);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

