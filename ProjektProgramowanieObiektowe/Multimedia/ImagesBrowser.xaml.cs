﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DataModels;
using ProjektProgramowanieObiektowe.Views;
using System.IO;

namespace ProjektProgramowanieObiektowe.Multimedia
{
    /// <summary>
    /// Logika interakcji dla klasy ImagesBrowser.xaml
    /// </summary>
    public partial class ImagesBrowser : Window
    {
        MyDirectory currentDirectory;
        MyFile currentImageFile;

        public ImagesBrowser(MyDirectory currentDirectory, MyFile imageFileToOpen)
        {
            InitializeComponent();
            this.currentDirectory = currentDirectory;
            this.currentImageFile = imageFileToOpen;
            Refresh(currentDirectory.Path);
            OpenImageFile(currentImageFile);
            GetAllDrives();
        }

        void GetAllDrives()
        {
            DriveInfo[] allDrives = DriveInfo.GetDrives();
            foreach (DriveInfo drive in allDrives)
            {
                drivesList.Items.Add(drive.Name);
            }
        }

        void Refresh(string path)
        {
            imageFileList.Children.Clear();

            if (currentDirectory.Path.Length < 4)
                backButton.IsEnabled = false;
            else
                backButton.IsEnabled = true;

            imageFilespath.Text = path;
            List<DiscElement> fileList = currentDirectory.ListDiscElements();
            foreach (DiscElement discElement in fileList)
            {
                if (discElement is MyDirectory)
                {
                    TextAndImageFileView imageView = new TextAndImageFileView(discElement);
                    imageFileList.Children.Add(imageView);
                    imageView.fileOpened += ImageView_fileOpened;
                }
                else if (discElement is MyFile)
                {
                    if (discElement.GetExtension() == ".png" || discElement.GetExtension() == ".jpg" || discElement.GetExtension() == ".bmp" || discElement.GetExtension() == ".ico")
                    {
                        TextAndImageFileView imageView = new TextAndImageFileView(discElement);
                        imageFileList.Children.Add(imageView);
                        imageView.fileOpened += ImageView_fileOpened;
                    }
                }
            }
        }


        void OpenImageFile(MyFile image)
        {
            this.currentImageFile = image;
            currentImageFileName.Text = currentImageFile.Name;
            imageContent.Source = new BitmapImage(new Uri(currentImageFile.Path));
        }


        private void ImageView_fileOpened(DiscElement discElement)
        {
            if (discElement is MyDirectory)
            {
                currentDirectory = new MyDirectory(discElement.Path);
                Refresh(currentDirectory.Path);
            }
            else
            {
                OpenImageFile((MyFile)discElement);
            }
        }


        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            int density = currentDirectory.Path.Count(x => x == '\\');
            if(density==1)
                currentDirectory = new MyDirectory(currentDirectory.Path.Replace(currentDirectory.Name, ""));
            else
                currentDirectory = new MyDirectory(currentDirectory.Path.Replace("\\" + currentDirectory.Name, ""));
            Refresh(currentDirectory.Path);
        }
        //<summary>
        //Zmiana podświetlenia, gdy mysz jest nad nazwą bierzącego pliku
        //</summary>
        private void ChangeColor(object sender, MouseEventArgs e)
        {
            currentImageFileName.Background = Brushes.LightBlue;
        }
        //<summary>
        //Zmiana podświetlenia, gdy mysz już nie jest nad nazwą bierzącego pliku
        //</summary>
        private void ChangeColorBack(object sender, MouseEventArgs e)
        {
            currentImageFileName.Background = Brushes.White;
        }
        //<summary>
        //Otwiera lokalizację bierzącego pliku
        //</summary>
        private void OpenImageDirectory(object sender, MouseButtonEventArgs e)
        {
            int density = currentImageFile.Path.Count(x => x == '\\');
            if (density == 1)
                currentDirectory = new MyDirectory(currentImageFile.Path.Replace(currentImageFile.Name, ""));
            else
                currentDirectory = new MyDirectory(currentImageFile.Path.Replace("\\" + currentImageFile.Name, ""));

            if (drivesList.Text != currentImageFile.Path.Remove(3) && drivesList.Text != "")
                drivesList.SelectedValue = currentImageFile.Path.Remove(3);

            Refresh(currentDirectory.Path);
        }
        //<summary>
        //Otwiera zawartość dysku
        //</summary>
        private void drivesList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currentDirectory = new MyDirectory(drivesList.SelectedItem.ToString());
            Refresh(currentDirectory.Path);
        }
    }
}
