﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DataModels;
using ProjektProgramowanieObiektowe.Views;
using System.IO;

namespace ProjektProgramowanieObiektowe.Multimedia
{
    /// <summary>
    /// Logika interakcji dla klasy TextBrowser.xaml
    /// </summary>
    public partial class TextBrowser : Window
    {
        MyDirectory currentDirectory;
        MyFile currentTextFile;
        public TextBrowser(MyDirectory currentDirectory,MyFile textFileToOpen)
        {
            InitializeComponent();
            this.currentDirectory = currentDirectory;
            this.currentTextFile = textFileToOpen;
            Refresh(currentDirectory.Path);
            OpenTextFile(textFileToOpen);
            GetAllDrives();
            AddFontSizes();
        }

        void GetAllDrives()
        {
            DriveInfo[] allDrives = DriveInfo.GetDrives();
            foreach (DriveInfo drive in allDrives)
            {
                drivesList.Items.Add(drive.Name);
            }
        }
        void AddFontSizes()
        {
            for (int i = 1; i <= 25; i++)
                fontSizes.Items.Add(i);
            fontSizes.SelectedValue = 12;
        }

        void Refresh(string path)
        {
            textFileList.Children.Clear();

            if (currentDirectory.Path.Length < 4)
                backButton.IsEnabled = false;
            else
                backButton.IsEnabled = true;

            textFilespath.Text = path;
            List<DiscElement> fileList = currentDirectory.ListDiscElements();
            foreach (DiscElement discElement in fileList)
            {
                if (discElement is MyDirectory)
                {
                    TextAndImageFileView textView = new TextAndImageFileView(discElement);
                    textFileList.Children.Add(textView);
                    textView.fileOpened += TextView_fileOpened;
                }
                else if (discElement is MyFile)
                {
                    if (discElement.GetExtension() == ".txt")
                    {
                        TextAndImageFileView textView = new TextAndImageFileView(discElement);
                        textFileList.Children.Add(textView);
                        textView.fileOpened += TextView_fileOpened;
                    }
                }
            }
        }

        private void TextView_fileOpened(DiscElement discElement)
        {
            if(discElement is MyDirectory)
            {
                currentDirectory = new MyDirectory(discElement.Path);
                Refresh(currentDirectory.Path);
            }
            else
            {
                OpenTextFile((MyFile)discElement);
            }
        }

        private void OpenTextFile(MyFile textFile)
        {
            this.currentTextFile = textFile;
            currentTextFileName.Text = currentTextFile.Name;
            FileStream stream = new FileStream(currentTextFile.Path,FileMode.Open);
            StreamReader streamReader = new StreamReader(stream, System.Text.Encoding.UTF8);
            textFileContent.Text = streamReader.ReadToEnd();
            stream.Close();
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            int density = currentDirectory.Path.Count(x => x == '\\');
            if (density == 1)
                currentDirectory = new MyDirectory(currentDirectory.Path.Replace(currentDirectory.Name, ""));
            else
                currentDirectory = new MyDirectory(currentDirectory.Path.Replace("\\" + currentDirectory.Name, ""));
            Refresh(currentDirectory.Path);
        }
        //<summary>
        //Otwiera lokalizację bierzącego pliku
        //</summary>
        private void openTextDirectory(object sender, MouseButtonEventArgs e)
        {
            int density = currentTextFile.Path.Count(x => x == '\\');
            if (density == 1)
                currentDirectory = new MyDirectory(currentTextFile.Path.Replace(currentTextFile.Name,""));
            else
                currentDirectory = new MyDirectory(currentTextFile.Path.Replace("\\" + currentTextFile.Name, ""));

            if (drivesList.Text != currentTextFile.Path.Remove(3) && drivesList.Text != "")
                drivesList.SelectedValue = currentTextFile.Path.Remove(3);

            Refresh(currentDirectory.Path);
        }
        //<summary>
        //Zmiana podświetlenia, gdy mysz jest nad nazwą bierzącego pliku
        //</summary>
        private void ChangeColor(object sender, MouseEventArgs e)
        {
            currentTextFileName.Background = Brushes.LightBlue;
        }
        //<summary>
        //Zmiana podświetlenia, gdy mysz już nie jest nad nazwą bierzącego pliku
        //</summary>
        private void ChangeColorBack(object sender, MouseEventArgs e)
        {
            currentTextFileName.Background = Brushes.LightGray;
        }
        //<summary>
        //Otwiera zawartość dysku
        //</summary>
        private void drivesList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currentDirectory = new MyDirectory(drivesList.SelectedItem.ToString());
            Refresh(currentDirectory.Path);
        }
        //<summary>
        //Zmienia wiekość czcionki
        //</summary>
        private void fontSizes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {            
            textFileContent.FontSize = (int)fontSizes.SelectedItem;
        }
    }
}
