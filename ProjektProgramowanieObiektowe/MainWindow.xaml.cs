﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DataModels;
using System.IO;
using ProjektProgramowanieObiektowe.Views;
using Microsoft.VisualBasic.FileIO;
using System.Diagnostics;
using System.Windows.Threading;
using ProjektProgramowanieObiektowe.PopingWindows;
using ProjektProgramowanieObiektowe.Multimedia;

namespace ProjektProgramowanieObiektowe
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        NewDirectoryName myWindow;
        MyDirectory currentDirectoryLeft;
        MyDirectory currentDirectoryRight;
        int leftCounter = 0;
        int rightCounter = 0;
        Stack<string> backingLeft = new Stack<string>();
        Stack<string> backingRight = new Stack<string>();

        public MainWindow()
        {
            InitializeComponent();
            GetAllDrives();
            backButtonRight.IsEnabled = false;
            backButtonLeft.IsEnabled = false;
            sortByCreationTimeLeft.IsEnabled = false;
            sortByCreationTimeRight.IsEnabled = false;
            sortyByNameLeft.IsEnabled = false;
            sortyByNameRight.IsEnabled = false;
            deleteChecked.IsEnabled = false;
            createDirectoryLeft.IsEnabled = false;
            createDirectoryRight.IsEnabled = false;
            copyCheckdLeft.IsEnabled = false;
            copyCheckedRight.IsEnabled = false;
        }
        void GetAllDrives()
        {
            DriveInfo[] allDrives = DriveInfo.GetDrives();
            foreach(DriveInfo drive in allDrives)
            {
                driveslistLeft.Items.Add(drive.Name);
                drivesListRight.Items.Add(drive.Name);
            }
        }



        private void LeftRefresh(string path,string sortType = null)
        {
            if (backingLeft.Count == 0)
                backButtonLeft.IsEnabled = false;
            else
                backButtonLeft.IsEnabled = true;

            createDirectoryLeft.IsEnabled = true;

            fileListLeft.Children.Clear();
            leftPath.Text = path;
            MyDirectory folder = new MyDirectory(path);
            List<DiscElement> fileList = folder.ListDiscElements(sortType);
            foreach(DiscElement discElement in fileList)
            {
                DiscElementView discElementView = new DiscElementView(discElement);
                fileListLeft.Children.Add(discElementView);
                discElementView.dirOpened += DiscElementView_LeftDirOpened;
                discElementView.dirDeleted += DiscElementView_LeftDirDeleted;
                discElementView.dirCopied += DiscElementView_LeftDirCopied;
                discElementView.dirChecked += DiscElementView_LeftDirChecked;
                if (discElement.GetExtension() == ".txt" || discElement.GetExtension() == ".png" || discElement.GetExtension() == ".jpg" || discElement.GetExtension() == ".bmp")
                {
                    discElementView.txtFileOpened += DiscElementView_LeftTxtImageFileOpened;
                }
            }
            FileSystemWatcher watcher = new FileSystemWatcher(currentDirectoryLeft.Path);
            watcher.EnableRaisingEvents = true;
            watcher.Created += Watcher_LeftDetectChange;
            watcher.Deleted += Watcher_LeftDetectChange;
            DiscElementView_LeftDirChecked(currentDirectoryLeft);
        }


        //<summary>
        //Odświerza, gdy wykryje zmianę w folderze
        //</summary>
        private void Watcher_LeftDetectChange(object sender, FileSystemEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(
            DispatcherPriority.Background,
            new Action(() =>
            {
                LeftRefresh(currentDirectoryLeft.Path);
                }));

        }
        private void DiscElementView_LeftDirOpened(DiscElement discElement)
        {
            backingLeft.Push(currentDirectoryLeft.Path);
            currentDirectoryLeft = (MyDirectory)discElement;
            leftPath.Text = currentDirectoryLeft.Path;
            LeftRefresh(currentDirectoryLeft.Path);
        }        
        private void DiscElementView_LeftDirChecked(DiscElement discElement)
        {
            leftCounter = 0;
            foreach(DiscElementView discElementView in fileListLeft.Children)
            {
                if(discElementView.checker.IsChecked == true)
                {
                    leftCounter++;
                }
            }
            if(leftCounter == 0 && rightCounter ==0)
            {
                deleteChecked.IsEnabled = false;
            }
            else
            {
                deleteChecked.IsEnabled = true;
            }
            if (leftCounter == 0)
            {
                checkedCounterLeft.Text = "";
                copyCheckdLeft.IsEnabled = false;
            }
            else
            {
                checkedCounterLeft.Text = "Zaznaczono: " + leftCounter.ToString();
                copyCheckdLeft.IsEnabled = true;
            }
        }
        private void DiscElementView_LeftDirDeleted(DiscElement discElement)
        {
            LeftRefresh(currentDirectoryLeft.Path);
        }
        private void DiscElementView_LeftDirCopied(DiscElement discElement)
        {
            try
            {
                CopyDiscElement(discElement, currentDirectoryRight.Path + "\\" + discElement.Name, 1);
                RightRefresh(currentDirectoryRight.Path);
            }
            catch(Exception)
            {
                MessageBox.Show("Nie można przekopiować!");
            }
        }
        //<summary>
        //Pobranie nazwy folderu do stworzenia
        //</summary>
        private void createDirectoryLeft_Click(object sender, RoutedEventArgs e)
        {
            NewDirectoryName newDirName = new NewDirectoryName();
            this.myWindow = newDirName;
            newDirName.Show();
            newDirName.nameGot += NewDirName_LeftNameGot;
        }
        //<summary>
        //Tworzy folder po pobraniu nazwy dla niego
        //</summary>
        private void NewDirName_LeftNameGot(string name)
        {
            string path = currentDirectoryLeft.Path + "/" + name;
            if (Directory.Exists(path) || name == "")
            {
                MessageBox.Show("Nie można utworzyć folderu!");
            }
            else
            {
                Directory.CreateDirectory(path);
                LeftRefresh(currentDirectoryLeft.Path);
            }
            myWindow.Close();
        }
        //<summary>
        //Kopiuje zaznaczone pliki na prawą stronę
        //</summary>
        private void copyCheckdLeft_Click(object sender, RoutedEventArgs e)
        {
            int i = 1;
            try
            {
                foreach (DiscElementView discElementView in fileListLeft.Children)
                {
                    if (discElementView.checker.IsChecked == true)
                    {
                        CopyDiscElement(discElementView.DiscElement, currentDirectoryRight.Path + "\\" + discElementView.DiscElement.Name, i);
                        i++;
                    }
                }
                RightRefresh(currentDirectoryRight.Path);
            }
            catch(Exception)
            {
                MessageBox.Show("Nie można przekopiować!");
            }
        }
        private void goIntoDirectoryLeft_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                currentDirectoryLeft = new MyDirectory(leftPath.Text);
                LeftRefresh(currentDirectoryLeft.Path);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                createDirectoryLeft.IsEnabled = false;
                backingLeft.Clear();
                backButtonLeft.IsEnabled = false;
            }
        }

        
        private void sortyByNameLeft_Click(object sender, RoutedEventArgs e)
        {
            LeftRefresh(currentDirectoryLeft.Path, "name");
        }
        private void sortByCreationTimeLeft_Click(object sender, RoutedEventArgs e)
        {
            LeftRefresh(currentDirectoryLeft.Path, "creationTime");
        }
        private void backButtonLeft_Click(object sender, RoutedEventArgs e)
        {
            currentDirectoryLeft = new MyDirectory(backingLeft.Pop());
            LeftRefresh(currentDirectoryLeft.Path);
        }
        //<summary>
        //Otwiera zawartość dysku w lewym oknie
        //</summary>
        private void driveslistLeft_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currentDirectoryLeft = new MyDirectory(driveslistLeft.SelectedItem.ToString());
            backingLeft.Clear();
            LeftRefresh(currentDirectoryLeft.Path);
            sortByCreationTimeLeft.IsEnabled = true;
            sortyByNameLeft.IsEnabled = true;
            DriveInfo drive = new DriveInfo(driveslistLeft.SelectedItem.ToString());
            driveInfoLeft.Text = (float)((drive.TotalSize - drive.TotalFreeSpace)/(1024*1024*1024)) + "GB" +  "/" + (float)(drive.TotalSize / (1024 * 1024 * 1024)) + "GB";
        }
        //<summary>
        //Otwiera przeglądarkę plików tekstowych i zdjęć
        //</summary>
        private void DiscElementView_LeftTxtImageFileOpened(DiscElement discElement)
        {
            if (discElement.GetExtension() == ".txt")
            {
                TextBrowser textBrowser = new TextBrowser(currentDirectoryLeft, (MyFile)discElement);
                textBrowser.Show();
            }
            else
            {
                ImagesBrowser imagesBrwoser = new ImagesBrowser(currentDirectoryLeft, (MyFile)discElement);
                imagesBrwoser.Show();
            }
        }





        private void RightRefresh(string path,string sortType = null)
        {
            if (backingRight.Count == 0)
                backButtonRight.IsEnabled = false;
            else
                backButtonRight.IsEnabled = true;

            createDirectoryRight.IsEnabled = true;

            fileListRight.Children.Clear();
            MyDirectory folder = new MyDirectory(path);
            rightPath.Text = path;
            List<DiscElement> fileList = folder.ListDiscElements(sortType);
            foreach (DiscElement discElement in fileList)
            {
                DiscElementView discElementView = new DiscElementView(discElement);
                fileListRight.Children.Add(discElementView);
                discElementView.dirOpened += DiscElementView_RightDirOpened;
                discElementView.dirDeleted += DiscElementView_RightDirDeleted;
                discElementView.dirCopied += DiscElementView_RightDirCopied;
                discElementView.dirChecked += DiscElementView_RightDirChecked;
                if (discElement.GetExtension() == ".txt" || discElement.GetExtension() == ".png" || discElement.GetExtension() == ".jpg" || discElement.GetExtension() == ".bmp")
                {
                    discElementView.txtFileOpened += DiscElementView_RightTxtImageFileOpened;
                }
            }
            FileSystemWatcher watcher = new FileSystemWatcher(currentDirectoryRight.Path);
            watcher.EnableRaisingEvents = true;
            watcher.Created += Watcher_RightDetectChange;
            watcher.Deleted += Watcher_RightDetectChange;
            DiscElementView_RightDirChecked(currentDirectoryRight);
        }


        //<summary>
        //Odświerza, gdy wykryje zmianę w folderze
        //</summary>
        private void Watcher_RightDetectChange(object sender, FileSystemEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(
           DispatcherPriority.Background,
           new Action(() =>
           {
               RightRefresh(currentDirectoryRight.Path);
           }));
        }
        private void DiscElementView_RightDirOpened(DiscElement discElement)
        {
            this.backingRight.Push(currentDirectoryRight.Path);
            this.currentDirectoryRight = (MyDirectory)discElement;
            RightRefresh(currentDirectoryRight.Path);
        }
        private void DiscElementView_RightDirChecked(DiscElement discElement)
        {
            rightCounter = 0;
            foreach (DiscElementView discElementView in fileListRight.Children)
            {
                if (discElementView.checker.IsChecked == true)
                {
                    rightCounter++;
                }
            }
            if (leftCounter == 0 && rightCounter == 0)
            {
                deleteChecked.IsEnabled = false;
            }
            else
            {
                deleteChecked.IsEnabled = true;
            }
            if (rightCounter == 0)
            {
                checkedCounterRight.Text = "";
                copyCheckedRight.IsEnabled = false;
            }
            else
            {
                checkedCounterRight.Text = "Zaznaczono: " + rightCounter.ToString();
                copyCheckedRight.IsEnabled = true;
            }
        }
        private void DiscElementView_RightDirDeleted(DiscElement discElement)
        {
            RightRefresh(currentDirectoryRight.Path);
        }
        private void DiscElementView_RightDirCopied(DiscElement discElement)
        {
            try
            {
                CopyDiscElement(discElement, currentDirectoryLeft.Path + "\\" + discElement.Name, 1);
                LeftRefresh(currentDirectoryLeft.Path);
            }
            catch(Exception)
            {
                MessageBox.Show("Nie można przekopiować!");
            }
        }
        //<summary>
        //Pobranie nazwy folderu do stworzenia
        //</summary>
        private void createDirectoryRight_Click(object sender, RoutedEventArgs e)
        {
            NewDirectoryName newDirName = new NewDirectoryName();
            this.myWindow = newDirName;
            newDirName.Show();
            newDirName.nameGot += NewDirName_RightNameGot;
        }
        //<summary>
        //Tworzy folder po pobraniu nazwy dla niego
        //</summary>
        private void NewDirName_RightNameGot(string name)
        {
            string path = currentDirectoryRight.Path + "/" + name;
            if(Directory.Exists(path) || name == "")
            {
                MessageBox.Show("Nie można utworzyć folderu!");
            }
            else
            {
                Directory.CreateDirectory(path);
                RightRefresh(currentDirectoryRight.Path);
            }
            myWindow.Close();
        }


        
        private void backButtonRight_Click(object sender, RoutedEventArgs e)
        {
            currentDirectoryRight = new MyDirectory(backingRight.Pop());
            RightRefresh(currentDirectoryRight.Path);
        }
        private void sortByCreationTimeRight_Click(object sender, RoutedEventArgs e)
        {
            RightRefresh(currentDirectoryRight.Path, "creationTime");
        }
        private void sortyByNameRight_Click(object sender, RoutedEventArgs e)
        {
            RightRefresh(currentDirectoryRight.Path, "name");
        }        
        //<summary>
        //Kopiuje zaznaczone pliki na prawą stronę
        //</summary>
        private void copyCheckedRight_Click(object sender, RoutedEventArgs e)
        {
            int i = 1;
            try
            {
                foreach (DiscElementView discElementView in fileListRight.Children)
                {
                    if (discElementView.checker.IsChecked == true)
                    {
                        CopyDiscElement(discElementView.DiscElement, currentDirectoryLeft.Path + "\\" + discElementView.DiscElement.Name, i);
                        i++;
                    }
                }
                LeftRefresh(currentDirectoryLeft.Path);
            }
            catch (Exception)
            {
                MessageBox.Show("Nie można przekopiować");
            }
        }
        private void CopyDiscElement(DiscElement discElement , string dir,int amount)
        {
            if (discElement is MyFile)
            {
                try
                {
                    File.Copy(discElement.Path, dir,false);
                }
                catch (Exception ex)
                {
                    if (amount < 2)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
            else
            {
                try
                {
                    FileSystem.CopyDirectory(discElement.Path,dir,false);
                }
                catch (Exception ex)
                {
                    if (amount < 2)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }

        }   
        private void goIntoDirectoryRight_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                currentDirectoryRight = new MyDirectory(rightPath.Text);
                RightRefresh(currentDirectoryRight.Path);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                createDirectoryRight.IsEnabled = false;
                backingRight.Clear();
                backButtonRight.IsEnabled = false;
            }
        }
        //<summary>
        //Otwiera zawartośc dysku w prawym oknie
        //</summary>
        private void drivesListRight_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currentDirectoryRight = new MyDirectory(drivesListRight.SelectedItem.ToString());
            backingRight.Clear();
            RightRefresh(currentDirectoryRight.Path);
            sortByCreationTimeRight.IsEnabled = true;
            sortyByNameRight.IsEnabled = true;
            DriveInfo drive = new DriveInfo(drivesListRight.SelectedItem.ToString());
            driveInfoRight.Text = (float)((drive.TotalSize - drive.TotalFreeSpace) / (1024 * 1024 * 1024)) + "GB" + "/" + (float)(drive.TotalSize / (1024 * 1024 * 1024)) + "GB";
        }
        //<summary>
        //Otwiera przeglądarkę plików tekstowych i zdjęć
        //</summary>
        private void DiscElementView_RightTxtImageFileOpened(DiscElement discElement)
        {
            if (discElement.GetExtension() == ".txt")
            {
                TextBrowser textBrowser = new TextBrowser(currentDirectoryRight, (MyFile)discElement);
                textBrowser.Show();
            }
            else
            {
                ImagesBrowser imagesBrwoser = new ImagesBrowser(currentDirectoryRight, (MyFile)discElement);
                imagesBrwoser.Show();
            }
        }


        private void deleteChecked_Click(object sender, RoutedEventArgs e)
        {
            foreach(DiscElementView discElementView in fileListLeft.Children)
            {
                if(discElementView.checker.IsChecked == true)
                {
                    if (discElementView.DiscElement is MyFile)
                        FileSystem.DeleteFile(discElementView.DiscElement.Path, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin);
                    else
                        FileSystem.DeleteDirectory(discElementView.DiscElement.Path, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin);
                }
            }

            foreach (DiscElementView discElementView in fileListRight.Children)
            {
                if (discElementView.checker.IsChecked == true)
                {
                    if (discElementView.DiscElement is MyFile)
                    {
                        try
                        {
                            FileSystem.DeleteFile(discElementView.DiscElement.Path, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin);
                        }
                        catch (Exception)
                        {
                            continue;
                        }
                    }
                    else
                    {
                        try
                        {
                            FileSystem.DeleteDirectory(discElementView.DiscElement.Path, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin);
                        }
                        catch(Exception)
                        {
                            continue;
                        }
                    }
                }

            }
            try
            {
                LeftRefresh(currentDirectoryLeft.Path);
            }
            catch(Exception)
            {
            }
            try
            {
                RightRefresh(currentDirectoryRight.Path);
            }
            catch (Exception)
            {
            }
        }

        private void SearchEveryTap(object sender, TextChangedEventArgs e)
        {
            if (search.Text != "")
            {
                foreach (DiscElementView discElemetView in fileListLeft.Children)
                {
                    discElemetView.grid.Background = Brushes.White;
                }
                foreach (DiscElementView discElemetView in fileListRight.Children)
                {
                    discElemetView.grid.Background = Brushes.White;
                }

                foreach (DiscElementView discElemetView in fileListLeft.Children)
                {
                    if (discElemetView.fileName.Text.IndexOf(search.Text, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        discElemetView.grid.Background = Brushes.LightGreen;
                    }
                }
                foreach (DiscElementView discElemetView in fileListRight.Children)
                {
                    if (discElemetView.fileName.Text.IndexOf(search.Text, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        discElemetView.grid.Background = Brushes.LightGreen;
                    }
                }
            }
            else
            {
                foreach (DiscElementView discElemetView in fileListLeft.Children)
                {
                    discElemetView.grid.Background = Brushes.White;
                }
                foreach (DiscElementView discElemetView in fileListRight.Children)
                {
                    discElemetView.grid.Background = Brushes.White;
                }
            }
        }
    }
}
