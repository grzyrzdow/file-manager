﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.VisualBasic.FileIO;

namespace DataModels
{
    public class MyDirectory : DiscElement
    {
        public MyDirectory(string path) : base(path)
        {

        }

        public override string Name
        {
            get
            {
                return System.IO.Path.GetFileName(Path);
            }
        }

        public override DateTime CreationTime
        {
            get
            {
                return Directory.GetCreationTime(Path);
            }
        }
        //<summary>
        //Posortuj pliki w dany sposób
        //</summary>
        public List<MyFile> GetAllFiesAndSort(string sortType = null)
        {
            string[] files = Directory.GetFiles(Path);
            List<MyFile> fileList = new List<MyFile>();

            foreach (string filePath in files)
            {
                fileList.Add(new MyFile(filePath));
            }
            if (sortType == null)
            {
                return fileList;
            }
            else if (sortType == "name")
            {
                List<MyFile> sortedFileList = fileList.OrderBy(o => o.Name).ToList();
                return sortedFileList;
            }
            else
            {
                List<MyFile> sortedFileList = fileList.OrderBy(o => o.CreationTime).ToList();
                return sortedFileList;
            }
        }
        //<summary>
        //Posortuj foldery w dany sposób
        //</summary>
        public List<MyDirectory> GetAllDirectoriesAndSort(string sortType = null)
        {
            string[] directories = Directory.GetDirectories(Path);
            List<MyDirectory> directoriesList = new List<MyDirectory>();

            foreach (string directoryPath in directories)
            {
                directoriesList.Add(new MyDirectory(directoryPath));
            }
            if (sortType == null)
            {
                return directoriesList;
            }
            else if (sortType == "name")
            {
                List<MyDirectory> sortedDirectoryList = directoriesList.OrderBy(o => o.Name).ToList();
                return sortedDirectoryList;
            }
            else
            {
                List<MyDirectory> sortedDirectoryList = directoriesList.OrderBy(o => o.CreationTime).ToList();
                return sortedDirectoryList;
            }
        }
        //<summary>
        //Scal posortowane foldery i pliki do jednej listy
        //</summary>
        public List<DiscElement> ListDiscElements(string sortType = null)
        {
            List<DiscElement> allFilesAndDirectories = new List<DiscElement>();
            allFilesAndDirectories.AddRange(GetAllDirectoriesAndSort(sortType));
            allFilesAndDirectories.AddRange(GetAllFiesAndSort(sortType));
            return allFilesAndDirectories;
        }

        public override string GetExtension()
        {
            return "";
        }
    }
}
