﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DataModels
{
    public class MyFile : DiscElement
    {
        public MyFile(string path) : base(path)
        {

        }


        public override string Name
        {
            get
            {
                return System.IO.Path.GetFileName(Path);
            }
        }

        public override DateTime CreationTime
        {
            get
            {
                return File.GetCreationTime(Path);
            }
        }
        public override string GetExtension()
        {
            return System.IO.Path.GetExtension(Path);
        }

    }
}
