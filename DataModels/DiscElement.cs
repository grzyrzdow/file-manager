﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModels
{
    public abstract class DiscElement
    {
        protected string path;

        public DiscElement(string path)
        {
            this.path = path;
        }

        string name;

        DateTime creationTime;

        public abstract string Name
        {
            get;
        }
        public abstract DateTime CreationTime
        {
            get;
        }
        public string Path
        {
            get
            {
                return this.path;
            }
        }

        public abstract string GetExtension();
    }
}
